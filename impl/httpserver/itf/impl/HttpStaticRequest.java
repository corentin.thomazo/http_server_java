package httpserver.itf.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintStream;

import httpserver.itf.HttpRequest;
import httpserver.itf.HttpResponse;

/*
 * This class allows to build an object representing an HTTP static request
 */
public class HttpStaticRequest extends HttpRequest {
	static final String DEFAULT_FILE = "index.html";
	
	public HttpStaticRequest(HttpServer hs, String method, String ressname) throws IOException {
		super(hs, method, ressname);
	}
	
	public void process(HttpResponse resp) throws Exception {
		String resname= this.getRessname();
		if(resname.endsWith("/")) {
			resname=resname+DEFAULT_FILE;
		}
		String method= this.getMethod();
		if (method.equals("GET")) {
			File folder=this.m_hs.getFolder();
			try {
				File send = new File(folder,resname);
				FileInputStream oui= new FileInputStream(send);
				resp.setReplyOk();
				resp.setContentLength((int)send.length());
				resp.setContentType(this.getContentType(resname));
				PrintStream ps= resp.beginBody();
				ps.write(oui.readAllBytes());
				oui.close();
			}catch (Exception e){
				resp.setReplyError(404,"not found");
			}
				
		}else {
			resp.setReplyError(404,"GET not");
		}
	}

}
